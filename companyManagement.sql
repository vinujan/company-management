-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 19, 2016 at 11:14 AM
-- Server version: 5.6.28-0ubuntu0.15.10.1
-- PHP Version: 5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `companyManagement`
--

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL,
  `company_name` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `code` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `company_name`, `code`, `created_at`, `updated_at`) VALUES
(20, 'copany 1', 'pvt', '2016-04-19 05:32:57', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `progress`
--

CREATE TABLE IF NOT EXISTS `progress` (
  `id` int(11) NOT NULL,
  `project_unit_user_id` int(11) DEFAULT NULL,
  `status` varchar(9) DEFAULT NULL,
  `comment` varchar(10000) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `isAdmin` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `progress`
--

INSERT INTO `progress` (`id`, `project_unit_user_id`, `status`, `comment`, `updated_by`, `isAdmin`) VALUES
(27, 27, NULL, 'hi this is admin', NULL, 1),
(28, 28, NULL, 'hi', NULL, 0),
(29, 29, NULL, 'this is unit', NULL, 1),
(30, 30, NULL, 'hi', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `project_name` varchar(50) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `company_id`, `project_name`, `address`, `note`, `created_at`, `updated_at`) VALUES
(26, 20, 'sdsd', 'sdsd', 'sdsdsd', '2016-04-19 05:33:26', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `project_unit`
--

CREATE TABLE IF NOT EXISTS `project_unit` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `unit_name` varchar(50) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_unit`
--

INSERT INTO `project_unit` (`id`, `project_id`, `unit_name`, `note`, `created_at`, `updated_at`) VALUES
(24, 26, 'unit', 'dzasdsdssd', '2016-04-19 05:35:07', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `project_unit_user`
--

CREATE TABLE IF NOT EXISTS `project_unit_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `project_unit_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_unit_user`
--

INSERT INTO `project_unit_user` (`id`, `user_id`, `project_id`, `project_unit_id`, `created_at`, `updated_at`) VALUES
(27, 34, 26, NULL, '2016-04-19 05:33:26', '0000-00-00 00:00:00'),
(28, 35, 26, NULL, '2016-04-19 05:34:10', '0000-00-00 00:00:00'),
(29, 34, NULL, 24, '2016-04-19 05:35:07', '0000-00-00 00:00:00'),
(30, 36, NULL, 24, '2016-04-19 05:35:33', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `telephone_number` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(72) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `roles` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `telephone_number`, `password`, `username`, `roles`, `company_id`, `created_at`, `updated_at`, `is_active`) VALUES
(8, 'company1', 'company2', '0771231235', '$2y$13$H3GHo959EQvw6A4XzEZXa.BVopc7GhYBktvj0qJGKaO08DdpKWlr2', 'superadmin@superadmin', 'ROLE_SUPER_ADMIN', NULL, '2016-03-29 17:32:43', '2016-03-29 17:32:43', 0),
(34, 'asas', 'asas', '2323333333', '$2y$13$B44OSs0d6.33kmLtD/qr2OfZbKCUOQ/v.XHXcFJRE.G587JF0GHke', 'c@c', 'ROLE_ADMIN', 20, '2016-04-19 05:32:57', '0000-00-00 00:00:00', 0),
(35, 'project ', 'user', '2324444444', '$2y$13$m1cZLG2r1wlg5sxsXsZo2uJWuL9qMthugBXkGdwP.NjCjN8dwASma', 'p@p', 'ROLE_CUSTOMER', NULL, '2016-04-19 05:34:10', '0000-00-00 00:00:00', 0),
(36, 'sddddddddddddd', 'sdsdsd', '2323333333', '$2y$13$yN.HWEUs.lbD9kdb6ASOSO8gWxEb6DpI/gOUChIchtTlTm1jL75ru', 'u@u', 'ROLE_CUSTOMER', NULL, '2016-04-19 05:35:33', '0000-00-00 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `progress`
--
ALTER TABLE `progress`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_unit_user_id` (`project_unit_user_id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `project_unit`
--
ALTER TABLE `project_unit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `project_unit_user`
--
ALTER TABLE `project_unit_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `project_unit_id` (`project_unit_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `company_id` (`company_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `progress`
--
ALTER TABLE `progress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `project_unit`
--
ALTER TABLE `project_unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `project_unit_user`
--
ALTER TABLE `project_unit_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `progress`
--
ALTER TABLE `progress`
  ADD CONSTRAINT `progress_ibfk_1` FOREIGN KEY (`project_unit_user_id`) REFERENCES `project_unit_user` (`id`);

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`);

--
-- Constraints for table `project_unit`
--
ALTER TABLE `project_unit`
  ADD CONSTRAINT `project_unit_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`);

--
-- Constraints for table `project_unit_user`
--
ALTER TABLE `project_unit_user`
  ADD CONSTRAINT `project_unit_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `project_unit_user_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  ADD CONSTRAINT `project_unit_user_ibfk_3` FOREIGN KEY (`project_unit_id`) REFERENCES `project_unit` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
