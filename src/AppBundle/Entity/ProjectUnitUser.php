<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class ProjectUnitUser
{
    private $createdAt = 'CURRENT_TIMESTAMP';
    private $updatedAt = '0000-00-00 00:00:00';
    private $id;
    private $projectUnitID;
    private $projectID;
    private $userID;

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param string $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProjectUnitID()
    {
        return $this->projectUnitID;
    }

    /**
     * @param mixed $projectUnitID
     */
    public function setProjectUnitID($projectUnitID)
    {
        $this->projectUnitID = $projectUnitID;
    }

    /**
     * @return mixed
     */
    public function getProjectID()
    {
        return $this->projectID;
    }

    /**
     * @param mixed $projectID
     */
    public function setProjectID($projectID)
    {
        $this->projectID = $projectID;
    }

    /**
     * @return mixed
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * @param mixed $userID
     */
    public function setUserID($userID)
    {
        $this->userID = $userID;
    }


}

