<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Progress
{
    private $status;
    private $comment;
    private $updatedBy;
    private $id;
    private $projectUnitUser;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param mixed $updatedBy
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProjectUnitUser()
    {
        return $this->projectUnitUser;
    }

    /**
     * @param mixed $projectUnitUser
     */
    public function setProjectUnitUser($projectUnitUser)
    {
        $this->projectUnitUser = $projectUnitUser;
    }


}

