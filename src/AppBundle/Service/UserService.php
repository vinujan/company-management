<?php
use AppBundle\Entity\DummyUser;
use AppBundle\Entity\User;

require_once 'MysqlConnector.php';

function getUser($id){
    $connection = open_database_connection();
    $params=array( $id);
    $query = "select id, first_name, last_name, email, telephone_number, username from user WHERE  id=?;";
    $result=mysqli_prepared_query($connection,$query,"s",$params);
    close_database_connection($connection);
    $user = new User();
    $user->setId($result[0]['id']);
    $user->setFirstName($result[0]['first_name']);
    $user->setLastName($result[0]['last_name']);
    $user->setEmail($result[0]['email']);
    $user->setTelephoneNumber($result[0]['telephone_number']);
    $user->setUsername($result[0]['username']);
    return $user;
}

function updateUser(User $user){
    $connection = open_database_connection();

    $query = "UPDATE user SET first_name=? , last_name=?, telephone_number=?, username=? ,updated_at=CURRENT_TIMESTAMP WHERE id=?;";
    $params = array($user->getFirstName(),$user->getLastName(),$user->getTelephoneNumber(),$user->getUsername(),$user->getId());
    $result = mysqli_prepared_query($connection,$query,"sssss",$params);
    close_database_connection($connection);
    // should do succes of fail
    return $result[0];
}

function createUserDB(User $user){
    $connection = open_database_connection();

    $query = "INSERT INTO user (first_name,last_name,telephone_number,username,password,company_id,roles)
      VALUES (?,?,?,?,?,?,?);";
    $params = array($user->getFirstName(),$user->getLastName(),$user->getTelephoneNumber(),$user->getUsername(),
        $user->getPassword(),$user->getCompanyId(),$user->getRole());
    $result=mysqli_prepared_query($connection,$query,"sssssss",$params);
    close_database_connection($connection);
    return $result;
}
function createProjectUnitUserDB(User $user){
    $connection = open_database_connection();

    $query = "INSERT INTO user (first_name,last_name,telephone_number,username,password,roles)
      VALUES (?,?,?,?,?,?);";
    $params = array($user->getFirstName(),$user->getLastName(),$user->getTelephoneNumber(),$user->getUsername(),
        $user->getPassword(),$user->getRole());
    $result=mysqli_prepared_query($connection,$query,"ssssss",$params);
    if(!$result[0]){
        return false;
    }
    $query = 'SELECT max(id) FROM user';
    $result = mysqli_query($connection,$query);
    close_database_connection($connection);
    return mysqli_fetch_row($result)[0];
}

//for signin


function signInUserDB(User $user){
    $connection = open_database_connection();

    $password=$user->getPassword();
    $encryptedpass=md5($password);

    $params=array( $user->getUserName());

    $query = "SELECT  interface_id, password FROM user WHERE user_name=?;";
    $result=mysqli_prepared_query($connection,$query,"s",$params);
    close_database_connection($connection);

    if (count($result)==1){
        if ($result[0]['password']==$encryptedpass){
            return $result[0]['interface_id'];
        }
        else{
            return "fail";
        }
    }
    else{
        return "fail";
    }
}

function changePasswordDB($passwordToDB , $id){
    $connection = open_database_connection();

    $query = "UPDATE user SET password=? WHERE id=?;";
    $params = array($passwordToDB,$id);
    mysqli_prepared_query($connection,$query,"ss",$params);
    close_database_connection($connection);
    return "password changed";
}

function getUserDetailsDB($sort){
    $connection = open_database_connection();
    $query = "select username, roles from user ORDER BY ".$sort." ASC ;";
    $result = mysqli_query($connection,$query);
    close_database_connection($connection);
    $resultArray = array();
    array_push($resultArray,array('username','roles'));
    array_push($resultArray,array('user name','Department'));
    while ($row = mysqli_fetch_row($result)) {
//        if ($row[1]=='ROLE'){
//            $row[1]="Admin";
//        }
//        else{
//            $row[1]="Maintain";
//        }

        array_push($resultArray, $row);
    }
    return $resultArray;
}

function deleteUserDB($id){
    $connection = open_database_connection();
    $query = "DELETE FROM user WHERE username=?;";
    $result=mysqli_prepared_query($connection,$query,"s",array($id));
    close_database_connection($connection);
    return $result;
}

function getUserByCompanyId($companyId){
    $connection = open_database_connection();
    $params=array( $companyId);
    $query = "select id, first_name, last_name, username,telephone_number from user WHERE  company_id=?;";
    $result=mysqli_prepared_query($connection,$query,"s",$params);
    close_database_connection($connection);

    $user = new User();
    $user->setId($result[0]['id']);
    $user->setFirstName($result[0]['first_name']);
    $user->setLastName($result[0]['last_name']);
    $user->setUsername($result[0]['username']);
    $user->setTelephoneNumber($result[0]['telephone_number']);
    return $user;
}

function deleteUserByCompanyId($companyId){
    $connection = open_database_connection();
    $query = "DELETE FROM user WHERE company_id=?;";
    $result=mysqli_prepared_query($connection,$query,"s",array($companyId));
    close_database_connection($connection);
    return $result;
}

function updateUserByCompanyId(User $user,$companyId){
    $connection = open_database_connection();

    $query = "UPDATE user SET first_name=? , last_name=?, telephone_number=?, username=? ,password = ?,updated_at=CURRENT_TIMESTAMP WHERE company_id=?;";
    $params = array($user->getFirstName(),$user->getLastName(),$user->getTelephoneNumber(),$user->getUsername(),$user->getPassword(),$companyId);
    $result = mysqli_prepared_query($connection,$query,"ssssss",$params);
    close_database_connection($connection);
    return $result;
}
