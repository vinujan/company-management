<?php


use AppBundle\Entity\ProjectUnitUser;

require_once 'MysqlConnector.php';

function createPUUserDB(ProjectUnitUser $projectUnitUser, $type)
{//creation of ProjectUnit
    $connection = open_database_connection();
    if ($type[0] === 'p') {
        $projectUnitUser->setProjectID($type[1]);
        $query = "INSERT INTO project_unit_user (user_id,project_id) VALUES (?,?);";
    } elseif ($type[0] === 'u') {
        $projectUnitUser->setProjectUnitID($type[1]);
        $query = "INSERT INTO project_unit_user (user_id,project_unit_id) VALUES (?,?);";
    }
    $params = array($projectUnitUser->getUserID(),$type[1]);
    $result = mysqli_prepared_query($connection, $query, "ss", $params);
    close_database_connection($connection);
    return $result;
}

function getProjectUnitUserByUserId($userId){
    $connection = open_database_connection();
    $params=array( $userId);
    $query = "select project_id,project_unit_id from project_unit_user WHERE  user_id=?;";
    $result=mysqli_prepared_query($connection,$query,"s",$params);
    close_database_connection($connection);
    $projectUnitUser = new ProjectUnitUser();
    $projectUnitUser->setProjectID($result[0]['project_id']);
    $projectUnitUser->setProjectUnitID($result[0]['project_unit_id']);
    return $projectUnitUser;
}