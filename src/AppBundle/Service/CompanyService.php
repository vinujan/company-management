<?php

use AppBundle\Entity\Company;

require_once 'MysqlConnector.php';


function createCompanyDB(Company $company){
    $connection = open_database_connection();

    $query = "INSERT INTO company (company_name,code)
      VALUES (?,?);";
    $params = array($company->getName(),$company->getCode());
    mysqli_prepared_query($connection,$query,"ss",$params);
    $query = 'SELECT max(id) FROM company';
    $result = mysqli_query($connection,$query);
    close_database_connection($connection);
    return mysqli_fetch_row($result)[0];
}

function getCompanyAdmin(){
    $connection = open_database_connection();
    require_once('../src/AppBundle/Service/UserService.php');
    $query = "Select id,company_name, code, created_at ,updated_at from company;";
    $result = mysqli_query($connection,$query);
    $resultArray = array();
    while ($row = mysqli_fetch_row($result)) {
        $company = new Company();
        $company->setId($row[0]);
        $company->setName($row[1]);
        $company->setCode($row[2]);
        $company->setCreatedAt($row[3]);
        $company->setUpdatedAt($row[4]);
        $user = getUserByCompanyId($company->getId());
        array_push($resultArray,array($company,$user));
    }
    return $resultArray;
}

function updateCompany(Company $company)
{
    $connection = open_database_connection();

    $query = "UPDATE company SET company_name=? , code=? WHERE id=?;";
    $params = array($company->getName(),$company->getCode(),$company->getId());
    mysqli_prepared_query($connection,$query,"sss",$params);
    close_database_connection($connection);

}

function companyDetailDB($companyId){
    $connection = open_database_connection();
    $params=array( $companyId);
    $query = "select company_name, code from company WHERE  id=?;";
    $result=mysqli_prepared_query($connection,$query,"s",$params);
    close_database_connection($connection);
    $company = new Company();
    $company->setName($result[0]['company_name']);
    $company->setCode($result[0]['code']);
    return $company;
}

function deleteCompany($id){
    $connection = open_database_connection();
    $query = "DELETE FROM company WHERE id=?;";
    $result=mysqli_prepared_query($connection,$query,"s",array($id));
    close_database_connection($connection);
    return $result;
}

function getCompanyName($companyId){
    $connection = open_database_connection();
    $params=array( $companyId);
    $query = "select company_name from company WHERE  id=?;";
    $result=mysqli_prepared_query($connection,$query,"s",$params);
    close_database_connection($connection);
    return $result[0]['company_name'];
}