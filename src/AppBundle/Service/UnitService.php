<?php

use AppBundle\Entity\ProjectUnit;

require_once 'MysqlConnector.php';

function getAllUnitByProjectId($projectId){
    $connection = open_database_connection();
    $params=array( $projectId);
    $query = "select id, unit_name, note, created_at,updated_at from project_unit WHERE  project_id=?;";
    $result=mysqli_prepared_query($connection,$query,"s",$params);
    close_database_connection($connection);
    if(!$result) return $result;
    $resultArray = array();
    for($i = 0; $i < sizeof($result);$i++){
        $projectUnit = new ProjectUnit();
        $projectUnit->setId($result[$i]['id']);
        $projectUnit->setName($result[$i]['unit_name']);
        $projectUnit->setNote($result[$i]['note']);
        $projectUnit->setProjectId($projectId);
        $projectUnit->setCreatedAt($result[$i]['created_at']);
        $projectUnit->setUpdatedAt($result[$i]['updated_at']);
        array_push($resultArray,$projectUnit);
    }
    return $resultArray;
}

function createUnitDB(ProjectUnit $projectUnit){
    $connection = open_database_connection();

    $query = "INSERT INTO project_unit (project_id,unit_name,note)
      VALUES (?,?,?);";
    $params = array($projectUnit->getProjectId(),$projectUnit->getName(),$projectUnit->getNote());
    mysqli_prepared_query($connection,$query,"sss",$params);
    $query = 'select max(id) from project_unit;';
    $result = mysqli_query($connection,$query);
    $unit_id = mysqli_fetch_array( $result )['max(id)'];
    close_database_connection($connection);
    return $unit_id;
}

function getUnitById($id){
    $connection = open_database_connection();
    $params=array( $id);
    $query = "select project_id,unit_name, note, created_at,updated_at from project_unit WHERE  id=?;";
    $result=mysqli_prepared_query($connection,$query,"s",$params);
    close_database_connection($connection);
    $projectUnit = new ProjectUnit();
    $projectUnit->setId($id);
    $projectUnit->setProjectId($result[0]['project_id']);
    $projectUnit->setName($result[0]['unit_name']);
    $projectUnit->setNote($result[0]['note']);
    $projectUnit->setCreatedAt($result[0]['created_at']);
    $projectUnit->setUpdatedAt($result[0]['updated_at']);
    return $projectUnit;
}

function updateUnitNoteDB($id,$note){
    $connection = open_database_connection();

    $query = "UPDATE project_unit SET note=?, updated_at =CURRENT_TIMESTAMP WHERE id=?;";
    $params = array($note,$id);
    $result = mysqli_prepared_query($connection,$query,"ss",$params);
    close_database_connection($connection);
    return $result;
}