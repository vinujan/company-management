<?php

use AppBundle\Entity\Project;

require_once 'MysqlConnector.php';

function getAllProjectByCompanyId($companyId){
    $connection = open_database_connection();
    $params=array( $companyId);
    $query = "select id, project_name, address, note, created_at,updated_at from project WHERE  company_id=? ORDER BY created_at DESC ;";
    $result=mysqli_prepared_query($connection,$query,"s",$params);
    close_database_connection($connection);
    if(!$result) return $result;
    $resultArray = array();
    for($i = 0; $i < sizeof($result);$i++){
        $project = new Project();
        $project->setId($result[$i]['id']);
        $project->setName($result[$i]['project_name']);
        $project->setAddress($result[$i]['address']);
        $project->setNote($result[$i]['note']);
        $project->setCreatedAt($result[$i]['created_at']);
        $project->setUpdatedAt($result[$i]['updated_at']);
        array_push($resultArray,$project);
    }
    return $resultArray;
}
function createProjectDB(Project $project){
    $connection = open_database_connection();

    $query = "INSERT INTO project (company_id,project_name,address,note)
      VALUES (?,?,?,?);";
    $params = array($project->getCompanyId(),$project->getName(),$project->getAddress(),$project->getNote());
    mysqli_prepared_query($connection,$query,"ssss",$params);
    $query = 'select max(id) from project;';
    $result = mysqli_query($connection,$query);
    $project_id = mysqli_fetch_array( $result )['max(id)'];
    close_database_connection($connection);
    return $project_id;
}

function getProjectById($id){
    $connection = open_database_connection();
    $params=array( $id);
    $query = "select company_id,project_name, address, note, created_at,updated_at from project WHERE  id=?;";
    $result=mysqli_prepared_query($connection,$query,"s",$params);
    close_database_connection($connection);
    $project = new Project();
    $project->setId($id);
    $project->setCompanyId($result[0]['company_id']);
    $project->setName($result[0]['project_name']);
    $project->setAddress($result[0]['address']);
    $project->setNote($result[0]['note']);
    $project->setCreatedAt($result[0]['created_at']);
    $project->setUpdatedAt($result[0]['updated_at']);
    return $project;
}

function getProjectSizeByCompanyId($companyId){
    $connection = open_database_connection();
    $params=array( $companyId);
    $query = "select id, project_name, address, note, created_at,updated_at from project WHERE  company_id=?;";
    $result=mysqli_prepared_query($connection,$query,"s",$params);
    close_database_connection($connection);
    if(!$result) return $result;
    return sizeof($result);
}

function updateProjectNoteDB($id,$note){
    $connection = open_database_connection();

    $query = "UPDATE project SET note=? ,updated_at = CURRENT_TIMESTAMP WHERE id=?;";
    $params = array($note,$id);
    $result = mysqli_prepared_query($connection,$query,"ss",$params);
    close_database_connection($connection);
    return $result;
}