<?php

use AppBundle\Entity\Progress;
use AppBundle\Entity\Project;

require_once 'MysqlConnector.php';

function createCustomerProgressDB($user_id , $comment){
    $connection = open_database_connection();

    $query = "INSERT INTO progress (project_unit_user_id,comment)
      VALUES ((SELECT id FROM project_unit_user WHERE user_id = ? ),?);";
    $params = array($user_id,$comment);
    $result= mysqli_prepared_query($connection,$query,"ss",$params);
    close_database_connection($connection);
    return $result[0];
}

function createAdminProgressDB($user_id ,$comment,$isAdmin,$type){
    $connection = open_database_connection();
    if($type[0] === 'p')
        $query = "INSERT INTO progress (project_unit_user_id,comment,isAdmin)
      VALUES ((SELECT id FROM project_unit_user WHERE user_id = ? AND project_id = ?),?,?);";
    else
        $query = "INSERT INTO progress (project_unit_user_id,comment,isAdmin)
      VALUES ((SELECT id FROM project_unit_user WHERE user_id = ? AND project_unit_id = ?),?,?);";
    $params = array($user_id,$type[1],$comment,$isAdmin);
    $result= mysqli_prepared_query($connection,$query,"ssss",$params);
    close_database_connection($connection);
    return $result[0];
}




function getAllProgressByUnitId($unit_id){
    $connection = open_database_connection();
    $query = "SELECT first_name, comment,isAdmin FROM (progress LEFT JOIN project_unit_user ON progress.project_unit_user_id = project_unit_user.id) LEFT JOIN user ON user.id = user_id WHERE
project_unit_id = ?";
    $params = array($unit_id);
    $result = mysqli_prepared_query($connection,$query,"s",$params);
    close_database_connection($connection);
    $resultArray = array();
    for($i= 0;$i < sizeof($result); $i++){
        array_push($resultArray,array($result[$i]['first_name'] , $result[$i]['comment'], $result[$i]['isAdmin']));
    }
    return $resultArray;
}

function getAllProgressByProjectId($project_id){
    $connection = open_database_connection();
    $query = "SELECT first_name, comment,isAdmin FROM (progress LEFT JOIN project_unit_user ON progress.project_unit_user_id = project_unit_user.id) LEFT JOIN user ON user.id = user_id WHERE
project_id = ?";
    $params = array($project_id);
    $result = mysqli_prepared_query($connection,$query,"s",$params);
    close_database_connection($connection);
    $resultArray = array();
    for($i= 0;$i < sizeof($result); $i++){
        array_push($resultArray,array($result[$i]['first_name'] , $result[$i]['comment'], $result[$i]['isAdmin']));
    }
    return $resultArray;
}