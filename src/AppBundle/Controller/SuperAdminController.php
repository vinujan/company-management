<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 10/12/15
 * Time: 22:12
 */
namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use AppBundle\Entity\DummyUser;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class SuperAdminController extends Controller
{
    /**
     * @Route("superAdmin/createCompany", name="createCompany")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function createCompany(Request $request)
    {

        return new Response("<div class='col-xs-6 col-xs-offset-3'>
                <form  role='form' id='createCompanyForm' action='" . "superAdmin/createCompanySubmit" . "'>
                    <div id='company' class='form-group'>
                        <h1 align='center'>Create Company</h1>
                        <div class='form-group'>
                            <label for='company_name' class='control-label'>Company Name</label>
                            <input type='text' class='form-control' id='company_name' placeholder='Company Name' maxLength='50' required>
                        </div>
                        <div class='form-group'>
                            <label for='code' class='control-label'>Code</label>
                            <input type='text' class='form-control' id='code_' placeholder='Code' maxLength='15' required>
                        </div>
                        <div class='col-xs-offset-8 form-group'>
                            <button type='button' class='btn btn-primary' id='next'>Create Admin</button>
                        </div>
                    </div>
                    <div id='admin' style='display: none;' class='form-group'>
                        <h1 align='center'>Create Admin</h1>
                        <div class='form-group'>
                            <label for='first_name' class='control-label'>First Name</label>
                            <input type='text' class='form-control' id='first_name' placeholder='First Name' maxLength='50'  required>
                        </div>
                        <div class='form-group'>
                            <label for='last_name' class='control-label'>Last Name</label>
                            <input type='text' class='form-control' id='last_name' placeholder='Last Name' maxLength='50'  required>
                        </div>
                        <div class='form-group'>
                            <label for='inputEmail' class='control-label'>Email</label>
                            <input type='email' class='form-control' id='email' placeholder='Email' maxLength='255' data-error='Invalid Email!' required>
                            <div class='help-block with-errors'></div>
                        </div>
                        <div class='form-group'>
                            <label for='telephone_number' class='control-label'>Telephone Number</label>
                            <input type='text' class='form-control' id='telephone_number' placeholder='Telephone Number' maxLength='10' required>
                        </div>
                        <div class='form-group'>
                            <label for='password' class='control-label'>Password</label>
                            <input type='text' class='form-control' id='password_' placeholder='Password' maxLength='50' value='qwert' disabled>
                        </div>
                        <div class='form-group col-xs-offset-6'>
                            <button type='submit' class='btn btn-primary' id='createCompany'>Submit</button>
                        </div>
                    </div>
                </form>

            <script type='text/javascript'>
                $('#createCompanyForm').validator();
                $('#next').click(function(){
                    if((($('#company_name').val() !== '') && ($('#code_').val() !== ''))){
                        $('#company').hide();
                        $('#admin').show();
                    }else{
                        $().toastmessage('showToast', {
                                                          text     : 'Fill the empty fields!',
                                                          sticky   : true,
                                                          position : 'top-right',
                                                          type     : 'error',
                                                          close    : function () {console.log('toast is closed ...');}
                                                       });
                    }

                });
                $( '#createCompanyForm' ).submit(function( event ) {

                  // Stop form from submitting normally
                  event.preventDefault();

                  // Get some values from elements on the page:
                  var form = $( this ),
                   url = form.attr( 'action' );

                  // Send the data using post
                  var company_name = $('#company_name').val();
                  var code_ = $('#code_').val();
                  var firstNameInput = $('#first_name').val();
                  var lastNameInput = $('#last_name').val();
                  var emailInput = $('#email').val();
                  var telephone = $('#telephone_number').val();
                  var password_ = $('#password_').val();
                  var posting = $.post( url, { name: company_name, code : code_, first_name : firstNameInput, last_name : lastNameInput, username : emailInput, telephone_number : telephone, password :password_} );

                  // Put the results in a div
                  posting.done(function( data ) {
                        $().toastmessage('showSuccessToast', 'Success');
                        setTimeout(function(){
                            location.reload();
                        }, 2000);
                  });
                });
            </script>
            </div>"
        );
    }

    /**
     * @Route("superAdmin/createCompanySubmit", name="createCompanySubmit")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function createCompanySubmit(Request $request)
    {
        $user = new User();
        $company = new Company();
        $company->setName($request->request->get('name'));
        $company->setCode($request->request->get('code'));
        $user->setFirstName($request->request->get('first_name'));
        $user->setLastName($request->request->get('last_name'));
        $user->setTelephoneNumber($request->request->get('telephone_number'));
        $user->setUsername($request->request->get('username'));
        $user->setRoles('ROLE_ADMIN');

        $encoder = $this->get('security.password_encoder');
        $rawPassword = $request->request->get('password');
        $encodedPassword = $encoder->encodePassword(new User(), $rawPassword);
        $user->setPassword($encodedPassword);
        require_once('../src/AppBundle/Service/CompanyService.php');
        $company_id = createCompanyDB($company);
        $user->setCompanyId($company_id);
        require_once('../src/AppBundle/Service/UserService.php');
        $result = createUserDB($user);
        if(!$result[0]){
            deleteCompany($company_id);
        }
        return new Response($company_id);
    }


    /**
     * @Route("/superAdminHome",name="superAdminHome")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function showAdminHomePage()
    {
        require_once('../src/AppBundle/Service/CompanyService.php');
        $companyAdminList = getCompanyAdmin();//[[company,admib],[],[]]

        return $this->render('SuperAdmin.html.twig', array('companyAdmin' => $companyAdminList));
    }

    /**
     * @Route("superAdmin/editCompany",name="editCompany")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function editCompany(Request $request)
    {
        $company = new Company();
        $company->setId($request->request->get('id'));
        $company->setName($request->request->get('name'));
        $company->setCode($request->request->get('code'));
        require_once('../src/AppBundle/Service/CompanyService.php');
        updateCompany($company);

        $user = new User();
        $user->setFirstName($request->request->get('first_name'));
        $user->setLastName($request->request->get('last_name'));
        $user->setUsername($request->request->get('username'));
        $user->setTelephoneNumber($request->request->get('telephone_number'));

        $encoder = $this->get('security.password_encoder');
        $passwordToDB = $encoder->encodePassword($user,$request->request->get('password'));

        $user->setPassword($passwordToDB);
        require_once('../src/AppBundle/Service/UserService.php');
        updateUserByCompanyId($user,$company->getId());
        return new Response("");
    }

    /**
     * @Route("superAdmin/companyAdminDetail",name="companyAdminDetail")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function companyAdminDetail(Request $request)
    {
        $companyId = $request->request->get('id');
        require_once('../src/AppBundle/Service/CompanyService.php');
        $company = companyDetailDB($companyId);
        require_once('../src/AppBundle/Service/UserService.php');
        $user = getUserByCompanyId($companyId);
        $response = new JsonResponse();
        $response->setData(array(
            'company_name' => $company->getName(),
            'code' => $company->getCode(),
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'username' => $user->getUsername(),
            'telephone_number' => $user->getTelephoneNumber()
        ));
        return $response;
    }

    /**
     * @Route("superAdmin/deleteCompany",name="deleteCompany")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function deleteCompany(Request $request)
    {
        $id = $request->request->get('id');
        require_once('../src/AppBundle/Service/ProjectService.php');
        if (getProjectSizeByCompanyId($id)) {
            return new Response("Company contains projects!");
        } else {
            require_once('../src/AppBundle/Service/UserService.php');
            deleteUserByCompanyId($id);
            require_once('../src/AppBundle/Service/CompanyService.php');
            deleteCompany($id);
            return new Response("Delete successfully done!");
        }
    }
}