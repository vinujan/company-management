<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 10/12/15
 * Time: 22:12
 */
namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use AppBundle\Entity\DummyUser;
use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectUnit;
use AppBundle\Entity\ProjectUnitUser;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AdminController extends Controller
{
    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/adminHome",name="adminHome")
     */
    public function showAdminHomePage()
    {
        //render the comapny name here
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $company_id = $user->getCompanyId();
        require_once('../src/AppBundle/Service/CompanyService.php');
        $companyName = getCompanyName($company_id);
        return $this->render('adminHome.html.twig', array('companyName' => $companyName));
    }

    /**
     * @Route("/admin/showProjects",name="showProjects")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function showProjects()//this function will be called when document ready
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $company_id = $user->getCompanyId();
        require_once('../src/AppBundle/Service/ProjectService.php');
        $projects = getAllProjectByCompanyId($company_id);
            
        $htmlAddProject = "<div class='row' >
                                <div class='col-xs-3 col-xs-offset-9'>
                                    <button class='btn btn-success' data-toggle='modal'
                                                        data-target='#addProject-modal'>Add Project</button>
                                    <br>
                                    <br>
                                </div>
                            </div>
                            <div id='addProject-modal' class='modal fade' tabindex='-1' role='dialog' data-backdrop='false'
         aria-labelledby='myModalLabel'
         aria-hidden='true'>
        <br>
        <br>
        <div class='modal-dialog'>
            <div class='modal-content'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                    <h4 class='modal-title' id='myModalLabel'>Create Project</h4>
                </div>
                <form role='form' id='addProjectModalForm'>
                    <div class='modal-body edit-content editModal' id='modalEditIdButton'>
                        <div class='form-group'>
                            <input type='text' class='form-control' id='Project_name' placeholder='Project Name'
                                   maxLength='50' required>
                        </div>
                        <div class='form-group'>
                            <input type='text' class='form-control' id='Address' placeholder='Address'
                                   maxLength='50' required>
                        </div>
                        <div class='form-group'>
                            <textarea class='form-control' id='Note' placeholder='Note' rows='8' required></textarea>
                        </div>
                    </div>
                    <div class='modal-footer'>
                        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                        <button type='submit' class='btn btn-warning' id='modalEditButton'>Create</button>
                    </div>
                </form>
            </div>
        </div>
        <script type='text/javascript'>
        $( '#addProjectModalForm' ).submit(function( event ) {
            $('#addProject-modal').modal('toggle');
            // Stop form from submitting normally
            event.preventDefault();

            // Get some values from elements on the page:
            var form = $( this ),
                    url = 'admin/createProject';

            // Send the data using post
            var Project_name = $('#Project_name').val();
            var Address = $('#Address').val();
            var Note = $('#Note').val();
            var posting = $.post( url, { project_name: Project_name ,address: Address, note : Note} );

            // Put the results in a div
            posting.done(function( data ) {
                $().toastmessage('showSuccessToast', 'Success');

                $('#dom').empty();
                $.get('admin/showProjects').done(function (data) {
                    $('#dom').append(data);
                });
            });
        });
        </script>
    </div>";
        if (!$projects) {
            return new Response($htmlAddProject."<div class='row'><div class='col-xs-4 col-xs-offset-3 panel panel-warning'><div class='panel-body'>No project to display!</div></div></div>");
        }
        if (sizeof($projects) == 0) return new Response($htmlAddProject."<h3>Nothing to show</h3>");
        $htmlProjectDisplay = $this->getHtmlForProjects($projects);
        $responseForClickProject = "<script type='text/javascript'>
                                    $( '.project' ).click(function() {
                                      var index = this.id;
                                      var url = 'admin/projectPage';
                                      var posting = $.post( url, { id: index } );
                                      posting.done(function( data ) {
                                        $('#dom').empty();
                                        $('#dom').append(data);
                                      });
                                    });
                                    </script>";
        return new Response($htmlAddProject.$htmlProjectDisplay.$responseForClickProject);

    }

    /**
     * @Route("admin/createProject", name="createProject")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function createProject(Request $request){
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $company_id = $user->getCompanyId();
        $project = new Project();
        $project->setCompanyId($company_id);
        $project->setName($request->request->get('project_name'));
        $project->setAddress($request->request->get('address'));
        $project->setNote($request->request->get('note'));
        require_once('../src/AppBundle/Service/ProjectService.php');
        $project_id = createProjectDB($project);
        require_once('../src/AppBundle/Service/ProjectUnitUserService.php');
        $projectUnitUser = new ProjectUnitUser();
        $projectUnitUser->setUserID($user->getId());
        createPUUserDB($projectUnitUser,array('p',$project_id));
        return new Response('');
    }

    /**
     * @Route("admin/projectPage", name="projectPage")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function projectPage(Request $request){
        $index = $request->request->get('id');
        require_once('../src/AppBundle/Service/ProjectService.php');
        $project = getProjectById((int)$index);
        require_once('../src/AppBundle/Service/UnitService.php');
        $this->units = getAllUnitByProjectId($project->getId());
        $htmlAddUnit = "<div class='row'>
                                <div class='col-xs-2 col-xs-offset-8'>
                                    <button class='btn btn-default projectUnitUser' id='p ".$project->getId()."' data-toggle='modal'
                                                        data-target='#addUser-modal'>Add User</button>
                                </div>
                                <div class='col-xs-2 '>
                                    <button class='btn btn-default' data-toggle='modal'
                                                        data-target='#addUnit-modal'>Add Unit</button>
                                    <br>
                                    <br>
                                </div>
                            </div>
                            <div id='addUnit-modal' class='modal fade' tabindex='-1' role='dialog' data-backdrop='false'
                                 aria-labelledby='myModalLabel'
                                 aria-hidden='true'>
                                <br>
                                <br>
                                <div class='modal-dialog'>
                                    <div class='modal-content'>
                                        <div class='modal-header'>
                                            <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                            <h4 class='modal-title' id='myModalLabel'>Create Unit</h4>
                                        </div>
                                        <form role='form' id='addUnitModalForm'>
                                            <div class='modal-body edit-content editModal' id='modalEditIdButton'>
                                                <input type='hidden' id='projectIndex' value='".$project->getId()."'>
                                                <div class='form-group'>
                                                    <input type='text' class='form-control' id='Unit_name' placeholder='Unit Name'
                                                           maxLength='50' required>
                                                </div>
                                                <div class='form-group'>
                                                    <textarea class='form-control' id='Note' placeholder='Note'
                                                                        rows='8' required></textarea>
                                                </div>
                                            </div>
                                            <div class='modal-footer'>
                                                <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                                                <button type='submit' class='btn btn-warning' id='modalEditButton'>Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <script type='text/javascript'>
                                $( '#addUnitModalForm' ).submit(function( event ) {
                                    $('#addUnit-modal').modal('toggle');
                                    // Stop form from submitting normally
                                    event.preventDefault();

                                    // Get some values from elements on the page:
                                    var form = $( this ),
                                            url = 'admin/createUnit';
                                    // Send the data using post
                                    var Unit_name = $('#Unit_name').val();
                                    var Note = $('#Note').val();
                                    var projectIndex = $('#projectIndex').val();
                                    var posting = $.post( url, { id:projectIndex,unit_name: Unit_name , note : Note} );

                                    // Put the results in a div
                                    posting.done(function( data ) {

                                        $().toastmessage('showSuccessToast', 'Success');
                                        $.post('admin/projectPage',{id:data}).done(function (data) {
                                            $('#dom').empty();
                                            $('#dom').append(data);
                                        });
                                    });
                                });
                                </script>
                            </div>
                            <div id='addUser-modal' class='modal fade' tabindex='-1' role='dialog' data-backdrop='false'
                                 aria-labelledby='myModalLabel'
                                 aria-hidden='true'>
                                <br>
                                <br>
                                <div class='modal-dialog'>
                                    <div class='modal-content'>
                                        <div class='modal-header'>
                                            <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                            <h4 class='modal-title' id='myModalLabel'>Create User</h4>
                                        </div>
                                        <form role='form' id='addUserModalForm'>
                                            <div class='form-group'>
                                                <input type='text' class='form-control' id='first_name' placeholder='First Name' maxLength='50'  required>
                                            </div>
                                            <div class='form-group'>
                                                <input type='text' class='form-control' id='last_name' placeholder='Last Name' maxLength='50'  required>
                                            </div>
                                            <div class='form-group'>
                                                <input type='email' class='form-control' id='email' placeholder='Email' maxLength='255' data-error='Invalid Email!' required>
                                                <div class='help-block with-errors'></div>
                                            </div>
                                            <div class='form-group'>
                                                <input type='text' class='form-control' id='telephone_number' placeholder='Telephone Number' maxLength='10' required>
                                            </div>
                                            <div class='form-group'>
                                                <input type='text' class='form-control' id='password_' placeholder='Password' maxLength='50' value='qwert' disabled>
                                            </div>
                                            <div class='col-xs-offset-6 form-group'>
                                                <button type='submit' class='btn btn-primary' id='update'>Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <script type='text/javascript'>
                                $('#addUserModalForm').validator();

                                $( '#addUserModalForm' ).submit(function( event ) {
                                    $('#addUser-modal').modal('toggle');
                                    // Stop form from submitting normally
                                    event.preventDefault();

                                    // Get some values from elements on the page:
                                    var form = $( this ),
                                            url = 'admin/createProjectUnitUser';
                                    // Send the data using post
                                      var firstNameInput = $('#first_name').val();
                                      var lastNameInput = $('#last_name').val();
                                      var emailInput = $('#email').val();
                                      var telephone = $('#telephone_number').val();
                                      var password_ = $('#password_').val();
                                      var posting = $.post( url, { type: $('.projectUnitUser').attr('id'),  first_name : firstNameInput, last_name : lastNameInput, username : emailInput, telephone_number : telephone,password :password_} );

                                    // Put the results in a div
                                    posting.done(function( data ) {

                                        $().toastmessage('showNoticeToast', data);
                                    });
                                });
                                </script>
                            </div>";
        return new Response($htmlAddUnit.$this->getHtmlForUnits($this->units,$project));
    }

    /**
     * @Route("admin/createUnit", name="createUnit")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function createUnit(Request $request){
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $projectUnit = new ProjectUnit();
        $projectUnit->setProjectId($request->request->get('id'));
        $projectUnit->setName($request->request->get('unit_name'));
        $projectUnit->setNote($request->request->get('note'));
        require_once('../src/AppBundle/Service/UnitService.php');
        $unitId = createUnitDB($projectUnit);
        require_once('../src/AppBundle/Service/ProjectUnitUserService.php');
        $projectUnitUser = new ProjectUnitUser();
        $projectUnitUser->setUserID($user->getId());
        createPUUserDB($projectUnitUser,array('u',$unitId));
        return new Response($request->request->get('id'));
    }

    /**
     * @Route("admin/unitDisplay", name="unitDisplay")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function unitDisplay(Request $request){
        require_once('../src/AppBundle/Service/UnitService.php');
        $id = $request->request->get('id');
        $unit = getUnitById($id);
        return new Response("<div class='row'>
                                <div class='panel panel-default'>
                                    <div class='panel-heading'>
                                        <h3 style='text-align: center;'>".$unit->getName()."</h3>
                                    </div>
                                    <div class='panel-body' style='text-align: center;' >
                                        <p  id = 'noteInProjectUnit' >".$unit->getNote()."</p>
                                        <button class='btn btn-success' id='buttonUpdateProject' >Edit</button>
                                    </div>
                                    <div class='panel-footer'>
                                        <div class='panel-footer'>
                                            <h3 style='text-align: center;'>Unit</h3>
                                            <div class='row'>
                                                <div class='col-xs-6'>
                                                    <p>Created At : ".$unit->getCreatedAt()."</p>
                                                </div>
                                                <div class='col-xs-6'>
                                                    <p>Updated At : ".$unit->getUpdatedAt()."</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script type='text/javascript'>
                            $( '#buttonUpdateProject' ).click(function() {
                                  if( $( '#buttonUpdateProject' ).text() === 'Edit'){
                                      var note = $('#noteInProjectUnit').text();
                                     $( '#buttonUpdateProject' ).text('Update');
                                     $( '#noteInProjectUnit' ).replaceWith( '<textarea  id = \"noteInProjectUnitEdit\" class=\"form-control\" rows=\"8\" required></textarea>');
                                     $( '#noteInProjectUnitEdit' ).val(note);
                                  }else if($( '#buttonUpdateProject' ).text() === 'Update'){
                                     var note = $('#noteInProjectUnitEdit').val();
                                     $( '#buttonUpdateProject' ).text('Edit');
                                     $( '#noteInProjectUnitEdit' ).replaceWith('<p id = \"noteInProjectUnit\">'+note+'</p>');
                                     var index = $('.projectUnitUser').attr('id').split(' ')[1];
                                     if($('.projectUnitUser').attr('id').charAt(0) === 'u'){
                                         var url = 'admin/changeUnitNote'
                                     }else if($('.projectUnitUser').attr('id').charAt(0) === 'p'){
                                         var url = 'admin/changeProjectNote'
                                     }
                                     var posting = $.post(url,{id :index ,editNote : note});
                                  }
                             });
                            </script>".$this->getHtmlForComments($unit,false).$this->htmlForAdminComment());
    }

    /**
     * @Route("admin/projectDisplay", name="projectDisplay")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function projectDisplay(Request $request)
    {
        require_once('../src/AppBundle/Service/ProjectService.php');
        $id = $request->request->get('id');
        $project = getProjectById($id);
        return new Response('<div class="row">
                                <div class="panel panel-default">
                                    <div class="panel-heading"  style=\'text-align:center\'>
                                        <h3>'.$project->getName().'</h3>
                                        <p>Address : '.$project->getAddress().'</p>
                                    </div>
                                    <div class="panel-body"  style=\'text-align:center\'>
                                        <p id = "noteInProjectUnit">'.$project->getNote().'</p>
                                        <button class=\'btn btn-success\' id=\'buttonUpdateProject\'>Edit</button>
                                    </div>
                                    <div class="panel-footer">
                                        <h3>Project</h3>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <p>Created At : '.$project->getCreatedAt().'</p>
                                            </div>
                                            <div class="col-xs-6">
                                                <p>Updated At : '.$project->getUpdatedAt().'</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script type=\'text/javascript\'>
                            $( \'#buttonUpdateProject\' ).click(function() {
                                  if( $( \'#buttonUpdateProject\' ).text() === \'Edit\'){
                                      var note = $(\'#noteInProjectUnit\').text();
                                     $( \'#buttonUpdateProject\' ).text(\'Update\');
                                     $( \'#noteInProjectUnit\' ).replaceWith( \'<textarea  id = "noteInProjectUnitEdit" class="form-control" rows="8" required></textarea>\');
                                     $( \'#noteInProjectUnitEdit\' ).val(note);
                                  }else if($( \'#buttonUpdateProject\' ).text() === \'Update\'){
                                     var note = $(\'#noteInProjectUnitEdit\').val();
                                     $( \'#buttonUpdateProject\' ).text(\'Edit\');
                                     $( \'#noteInProjectUnitEdit\' ).replaceWith(\'<p id = "noteInProjectUnit">\'+note+\'</p>\');
                                     var index = $(\'.projectUnitUser\').attr(\'id\').split(\' \')[1];
                                     if($(\'.projectUnitUser\').attr(\'id\').charAt(0) === \'u\'){
                                         var url = \'admin/changeUnitNote\'
                                     }else if($(\'.projectUnitUser\').attr(\'id\').charAt(0) === \'p\'){
                                         var url = \'admin/changeProjectNote\'
                                     }
                                     var posting = $.post(url,{id :index ,editNote : note});
                                  }

                             });
                            </script>'.$this->getHtmlForComments($project,true).$this->htmlForAdminComment());
    }


    /**
     * @Route("admin/createProjectUnitUser", name="createProjectUnitUser")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function createProjectUnitUser(Request $request)
    {
        $user = new User();
        $user->setFirstName($request->request->get('first_name'));
        $user->setLastName($request->request->get('last_name'));
        $user->setTelephoneNumber($request->request->get('telephone_number'));
        $user->setUsername($request->request->get('username'));
        $user->setRoles('ROLE_CUSTOMER');

        $encoder = $this->get('security.password_encoder');
        $rawPassword = $request->request->get('password');
        $encodedPassword = $encoder->encodePassword(new User(),$rawPassword);
        $user->setPassword($encodedPassword);
        require_once('../src/AppBundle/Service/UserService.php');
        $id = createProjectUnitUserDB($user);
        if(!$id){
            return new Response('Email unavailable!');
        }
        require_once('../src/AppBundle/Service/ProjectUnitUserService.php');
        $projectUnitUser = new ProjectUnitUser();
        $projectUnitUser->setUserID($id);
        $type = explode(" ", $request->request->get('type'));
        createPUUserDB($projectUnitUser,$type);
        return new Response("Success");
    }





    public function getHtmlForProjects($projects){
        $result = "";
        for($i = 0; $i < sizeof($projects);$i = $i + 2){
            $result = $result . "<div class='row'>";
            for($j = 0; $j < 2;$j++){
                $result = $result. " <div class='col-xs-6 project' id='".$projects[$i+$j]->getId()."'>
                                <div class='panel panel-success'>
                                    <div class='panel-heading'  style='text-align:center'>
                                        <h4>".$projects[$i+$j]->getName()."</h4>
                                    </div>
                                    <div class='panel-body'  style='text-align:center'>
                                        <h3> Address : ".$projects[$i+$j]->getAddress()."</h3>
                                        <p>".$projects[$i+$j]->getNote()."</p>
                                    </div>
                                    <div class='panel-footer'>
                                        <h3>Project</h3>
                                        <div class='row'>
                                            <div class='col-xs-6'>
                                                <p>Created At : ".$projects[$i+$j]->getCreatedAt()."</p>
                                            </div>
                                            <div class='col-xs-6'>
                                                <p>Updated At : ".$projects[$i+$j]->getUpdatedAt()."</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>";
                if(($i+$j+1) == sizeof($projects) && sizeof($projects)%2==1) break;
            }
            $result = $result."</div>";
        }
        return $result;
    }

    public function getHtmlForUnits($units,$project){
        $result = '<div class="col-xs-3">
        <ul class="nav nav-pills nav-stacked">
        <h3>Project</h3>
        <li><a href="#" class="project" id='.$project->getId().'>'.$project->getName().'</a></li>
        <h3>Units</h3>';
        for($i = 0;$i < sizeof($units) ;$i++){
            $result =$result."<li><a href='#' class='unit' id='".$units[$i]->getId()."'>".$units[$i]->getName()."</a></li>";
        }
        $result =$result."</ul>
            </div>
            <div class='col-xs-9' id='projectUnitDisplayArea'>
                <div class='row'>
                    <div class='panel panel-default'>
                        <div class='panel-heading' style='text-align:center'>
                            <h3>".$project->getName()."</h3>
                            <p>Address : ".$project->getAddress()."</p>
                        </div>
                        <div class='panel-body' style='text-align:center'>
                            <div id = 'noteInProjectUnit'><p>".$project->getNote()."</p></div>
                            <button class='btn btn-success' id='buttonUpdateProject'>Edit</button>
                        </div>
                        <div class='panel-footer'>
                            <h3>Project</h3>
                                <div class='row'>
                                    <div class='col-xs-6'>
                                        <p>Created At : ".$project->getCreatedAt()."</p>
                                    </div>
                                    <div class='col-xs-6'>
                                        <p>Updated At : ".$project->getUpdatedAt()."</p>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>".$this->getHtmlForComments($project,true).$this->htmlForAdminComment().
            "</div>
            <script type='text/javascript'>
                $( '.unit' ).click(function() {
                      var index = this.id;
                      $('.projectUnitUser').attr('id', 'u ' + this.id);
                      var url = 'admin/unitDisplay';
                      var posting = $.post( url, { id: index } );
                      posting.done(function( data ) {
                        $('#projectUnitDisplayArea').empty();
                        $('#projectUnitDisplayArea').append(data);
                      });
                 });
                 $( '.project' ).click(function() {
                      var index = this.id;
                      $('.projectUnitUser').attr('id', 'p ' + this.id);
                      var url = 'admin/projectDisplay';
                      var posting = $.post( url, { id: index } );
                      posting.done(function( data ) {
                        $('#projectUnitDisplayArea').empty();
                        $('#projectUnitDisplayArea').append(data);
                      });
                 });
                 $( '#buttonUpdateProject' ).click(function() {
                      if( $( '#buttonUpdateProject' ).text() === 'Edit'){
                          var note = $('#noteInProjectUnit').text();
                         $( '#buttonUpdateProject' ).text('Update');
                         $( '#noteInProjectUnit' ).replaceWith( '<textarea  id = \"noteInProjectUnitEdit\" class=\"form-control\" rows=\"8\" required></textarea>');
                         $( '#noteInProjectUnitEdit' ).val(note);
                      }else if($( '#buttonUpdateProject' ).text() === 'Update'){
                         var note = $('#noteInProjectUnitEdit').val();
                         $( '#buttonUpdateProject' ).text('Edit');
                         $( '#noteInProjectUnitEdit' ).replaceWith('<p id = \"noteInProjectUnit\">'+note+'</p>');
                         var index = $('.projectUnitUser').attr('id').split(' ')[1];
                         if($('.projectUnitUser').attr('id').charAt(0) === 'u'){
                             var url = 'admin/changeUnitNote'
                         }else if($('.projectUnitUser').attr('id').charAt(0) === 'p'){
                             var url = 'admin/changeProjectNote'
                         }
                         var posting = $.post(url,{id :index ,editNote : note});
                      }
                 });
            </script>";
        return $result;
    }

    public function getHtmlForComments($project, $isPorject){
        $result = "";
        require_once('../src/AppBundle/Service/ProgressService.php');
        if($isPorject){
            $comments = getAllProgressByProjectId($project->getId());
        }else{
            $comments = getAllProgressByUnitId($project->getId());
        }
        for($i = 0;$i < sizeof($comments);$i++){
            $result = $result."<div class='row'>
                <div class='panel panel-warning'>
                    <div class='panel-heading'>Name : ".$comments[$i][0]."</div>
                    <div class='panel-body'>Comment : ".$comments[$i][1]."</div>
                </div>
            </div>";
        }
        return $result;
    }

    /**
     * @Route("admin/changeProjectNote", name="changeProjectNote")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function changeProjectNote(Request $request)
    {
        $id = $request->request->get('id');
        $note = $request->request->get('editNote');
        require_once('../src/AppBundle/Service/ProjectService.php');
        updateProjectNoteDB($id,$note);
        return new Response("");
    }

    /**
     * @Route("admin/changeUnitNote", name="changeUnitNote")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function changeUnitNote(Request $request)
    {
        $id = $request->request->get('id');
        $note = $request->request->get('editNote');
        require_once('../src/AppBundle/Service/UnitService.php');
        updateUnitNoteDB($id,$note);
        return new Response("");

    }


    public function htmlForAdminComment(){
        return "<div class='row' id='panelComment'>
            <div class='panel panel-default'>
                <div class='panel-heading'>
                    <h4>Add Comment</h4>
                </div>
                <form role='form' id='formComment'>
                    <div class='panel-body'>
                        <div class='form-group'>
                            <label for=\"comment\">Comment</label>
                            <textarea class='form-control' rows='8' id='comment' required></textarea>
                        </div>
                    </div>
                    <div class='panel-footer'>
                        <button class='btn btn-default' id='btnComment' type='submit'>Comment</button>
                    </div>
                </form>
            </div>
            <script type='text/javascript'>
                $('#formComment').submit(function (event) {

                    // Stop form from submitting normally
                    event.preventDefault();

                    // Get some values from elements on the page:
                    var form = $(this),
                            url = 'comment';
                    // Send the data using post
                    var comment_ = $('#comment').val();
                    var posting = $.post(url, {comment: comment_,project_unit_id:$('.projectUnitUser').attr('id')});

                    // Put the results in a div
                    posting.done(function (data) {
                        $().toastmessage('showSuccessToast', 'Success');
                        $(\"<div class='row' > <div class='panel panel-default'><div class='panel-heading'>You</div><div class='panel-body'>\" + comment_ + \"</div> </div> </div>\").insertBefore('#panelComment');
                        console.log(data);
                        $('#comment').val('');
                    });
                });
            </script>
        </div>";
    }
}










