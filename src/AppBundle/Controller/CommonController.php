<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 10/12/15
 * Time: 22:12
 */
namespace AppBundle\Controller;

use AppBundle\Entity\DummyUser;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class CommonController extends Controller
{
    /**
     * @Route("/", name="login_route")
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'security/login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error' => $error,
            )
        );
    }


    /**
     * @Route("/login_check", name="login_check")
     */
    public function loginCheckAction()
    {
        // this controller will not be executed,
        // as the route is handled by the Security system
    }

    /**
     * @Route("/logging_in")
     */
    public function loggingIn(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        switch ($user->getRoles()[0]) {
            case "ROLE_SUPER_ADMIN":
                return $this->redirectToRoute("superAdminHome");
                break;
            case "ROLE_ADMIN":
                return $this->redirectToRoute("adminHome");
                break;
            case "ROLE_CUSTOMER":
                return $this->redirectToRoute("customerHome");
                break;
            default:
                return $this->redirectToRoute("login_route");
        }
    }


    /**
     * @Route("/logout", name="logout")
     */
    public function logout(Request $request)
    {
        //log out
    }


    /**
     * @Route("/updateAccount", name="updateAccount")
     */
    public function updateAccount(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        return new Response(
            "<div class='col-xs-6 col-xs-offset-3'>
                <form  role='form' id='updateAccount' action='" . "updateAccountSubmit" . "'>
                    <div class='form-group'>
                        <label for='first_name' class='control-label'>First Name</label>
                        <input type='text' class='form-control' id='first_name' placeholder='First Name' maxLength='50' value='" . $user->getFirstName() . "' required>
                    </div>
                    <div class='form-group'>
                        <label for='last_name' class='control-label'>Last Name</label>
                        <input type='text' class='form-control' id='last_name' placeholder='Last Name' maxLength='50' value='" . $user->getLastName() . "' required>
                    </div>
                    <div class='form-group'>
                        <label for='inputEmail' class='control-label'>Email</label>
                        <input type='email' class='form-control' id='email' placeholder='Email' maxLength='255' data-error='Invalid Email!' value='" . $user->getUsername() . "' required>
                        <div class='help-block with-errors'></div>
                    </div>
                    <div class='form-group'>
                        <label for='telephone_number' class='control-label'>Telephone Number</label>
                        <input type='text' class='form-control' id='telephone_number' placeholder='Telephone Number' maxLength='10' value='" . $user->getTelephoneNumber() . "' required>
                    </div>
                    <div class='col-xs-offset-6 form-group'>
                        <button type='submit' class='btn btn-primary' id='update'>Submit</button>
                    </div>
                </form>

            <script type='text/javascript'>
                $('#updateAccount').validator();
                $( '#updateAccount' ).submit(function( event ) {
 
                  // Stop form from submitting normally
                  event.preventDefault();
                 
                  // Get some values from elements on the page:
                  var form = $( this ),
                   url = form.attr( 'action' );
                 
                  // Send the data using post
                  var firstNameInput = $('#first_name').val();
                  var lastNameInput = $('#last_name').val();
                  var emailInput = $('#email').val();
                  var telephone = $('#telephone_number').val();
                  var userName = $('#username').val();
                  var posting = $.post( url, { first_name : firstNameInput, last_name : lastNameInput, username : emailInput, telephone_number : telephone} );
                 
                  // Put the results in a div
                  posting.done(function( data ) {
//                        $(\"#modifyAccount\").click();
                        if(data === 'True'){
                            $().toastmessage('showNoticeToast', 'Success');
                            setTimeout(function(){
                                location.reload();
                            }, 2000);
                        }
                        else{
                            $('#email').val('');
                            $().toastmessage('showErrorToast', 'email you entered is unavailable!');
                        }
                  });
                });
            </script>
            </div>"
        );
    }

    /**
     * @Route("/updateAccountSubmit", name="updateAccountSubmit")
     */
    public function updateAccountSubmit(Request $request)
    {
        $userOld = $this->get('security.token_storage')->getToken()->getUser();
        $user = new User();
        $user->setId($userOld->getId());
        $user->setFirstName($request->request->get('first_name'));
        $user->setLastName($request->request->get('last_name'));
        $user->setTelephoneNumber($request->request->get('telephone_number'));
        $user->setUsername($request->request->get('username'));
        require_once('../src/AppBundle/Service/UserService.php');
        $result = updateUser($user);
        if ($result)
            return new Response("True");
        else
            return new Response("False");
    }

    /**
     * @Route("/changePassword", name="changePassword")
     */
    public function changePassword(Request $request)
    {
        return new Response("
            <div class='col-xs-6 col-xs-offset-3'>
                <form role='form' id='changePasswordForm' action='" . "changePasswordSubmit" . "' >
                    <div class='form-group'>
                        <label for='inputPassword' class='control-label'>Old Password</label>
                        <input type='password' class='form-control' id='oldInputPassword' placeholder='Old Password' required>
                    </div>
                    <div class='form-group'>
                        <label for='inputPassword' class='control-label'>New Password</label>
                        <div class='row'>
                            <div class='form-group col-xs-6'>
                                 <input type='password' data-minlength='6' class='form-control' id='inputPassword' placeholder='New Password' required>
                                 <div class='help-block'>Minimum of 6 characters</div>
                            </div>
                            <div class='form-group col-xs-6'>
                                <input type='password' class='form-control' id='inputPasswordConfirm' data-match='#inputPassword' data-match-error='password not matching' placeholder='Confirm Password' required>
                                <div class='help-block with-errors'></div>
                            </div>
                        </div>
                    </div>
                    <div class='col-xs-offset-4 form-group'>
                        <button type='submit' class='btn btn-primary' id='changePassword'>Submit</button>
                    </div>
                </form>

            <script type='text/javascript'>
                $('#changePasswordForm').validator();
                $( '#changePasswordForm' ).submit(function( event ) {

                  // Stop form from submitting normally
                  event.preventDefault();

                  // Get some values from elements on the page:
                  var form = $( this ),
                   url = form.attr( 'action' );

                  // Send the data using post
                  var oldInputPassword = $('#oldInputPassword').val();
                  var inputPassword = $('#inputPassword').val();
                  var posting = $.post( url, { old_password : oldInputPassword, password : inputPassword} );

                  // Put the results in a div
                  posting.done(function( data ) {
                        if(data === 'false'){
                            $().toastmessage('showErrorToast', 'Wrong password');
                            $('#changePassword').click();
                        }else{
                            $().toastmessage('showSuccessToast', 'Password changed successfully');
                            setTimeout(function(){
                                location.reload();
                            }, 2000);
                        }
                  });
                });
            </script>
            </div>"
        );
    }

    /**
     * @Route("/changePasswordSubmit", name="changePasswordSubmit")
     */
    public function changePasswordSubmit(Request $request)
    {
        $user = new User();
        $id = $request->request->get('id');
        $password = $request->request->get('password');
        $oldPassword = $request->request->get('old_password');
        $encoder = $this->get('security.password_encoder');
        $u = $this->get('security.token_storage')->getToken()->getUser();
        $result = $encoder->isPasswordValid($u, $oldPassword);
        if ($result) {
            require_once('../src/AppBundle/Service/UserService.php');
            $passwordToDB = $encoder->encodePassword($user, $password);
            changePasswordDB($passwordToDB, $id);
            return new Response("true");
        } else {
            return new Response("false");
        }
    }

    /**
     * @Route("/accessDenied", name="accessDenied")
     */
    public function accesDeniedPage(Request $request)
    {
        return $this->render('access_denied.html.twig');
    }
}