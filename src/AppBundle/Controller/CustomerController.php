<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 10/12/15
 * Time: 22:12
 */
namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use AppBundle\Entity\DummyUser;
use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectUnit;
use AppBundle\Entity\ProjectUnitUser;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class CustomerController extends Controller
{
    /**
     * @Route("/customerHome",name="customerHome")
     * @Security("has_role('ROLE_CUSTOMER')")
     */
    public function showAdminHomePage()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $id = $user->getId();
        require_once('../src/AppBundle/Service/ProjectUnitUserService.php');
        $projectUnitUser = getProjectUnitUserByUserId($id);
        if($projectUnitUser->getProjectID() == null){
            require_once('../src/AppBundle/Service/UnitService.php');
            $unit = getUnitById($projectUnitUser->getProjectUnitID());
            require_once('../src/AppBundle/Service/ProgressService.php');
            $comments = getAllProgressByUnitId($projectUnitUser->getProjectUnitID());
            return $this->render('customer.html.twig',array('isProject' => false,'unit'=>$unit,'comments'=>$comments));
        }else{
            require_once('../src/AppBundle/Service/ProjectService.php');
            $project = getProjectById($projectUnitUser->getProjectID());
            require_once('../src/AppBundle/Service/ProgressService.php');
            $comments = getAllProgressByProjectId($projectUnitUser->getProjectID());
            return $this->render('customer.html.twig',array('isProject' => true,'project'=>$project,'comments'=>$comments));
        }

    }


    /**
     * @Route("comment", name="comment")
     */
    public function comment(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $comment = $request->request->get('comment');
        $id = $user->getId();
        $type = explode(" ", $request->request->get('project_unit_id'));
        require_once('../src/AppBundle/Service/ProgressService.php');
        if($user->getRole() === 'ROLE_ADMIN'){
            $result = createAdminProgressDB($id,$comment,1,$type);
        }else{
            $result = createCustomerProgressDB($id,$comment);
        }
        return new Response($result);
    }

}